import Vue from 'vue'
import VueRouter from 'vue-router'
import firebaseApp from '../firebaseInit'
import store from '../store/index'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Index.vue')
  },
  {
    path: '/afiche',
    name: 'afiche',
    component: () => import('../views/afiches/Afiche.vue'),
    props: true,
    meta: { requiresAuth: true }
  },
  {
    path: '/bienvenido',
    name: 'bienvenido',
    component: () => import('../views/Bienvenida.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/*',
    name: '404',
    component: () => import('../views/Index.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    if (!store.getters.sessionActiva){
      next({ path: '/' })
    }        
    else{
      next()
    }
  }
  else{
    next()
  }
})

export default router
