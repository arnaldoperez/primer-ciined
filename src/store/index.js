import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import firebase from './firebase'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user:null
  },
  mutations: {
    setUser(state, user){
      state.user=user
    }
  },
  getters:{
    sessionActiva:state=>{
      return !!state.user
    }
  },
  actions: {
  },
  modules: {
    firebase
  }
})
