const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({
	origin: true,
});
const rp = require('request-promise')

const moment = require('moment')


let serviceAccount = require('./firebaseKey.json');

//admin.initializeApp({ credential: admin.credential.cert(serviceAccount) });
admin.initializeApp()
const db = admin.firestore();

exports.newUser = functions.https.onRequest((request, response) => {
	return cors(request, response, () => {
		// Si no es una peticion post se descarta
		if (request.method !== 'POST') {
			response.status(405).send('Metodo no disponible');
		}
		else {
			// Verifica si la cedula ya esta registrada
			db.collection('usuarios').where('cedula', '==', request.body['cedula']).get()
			.then(data => {
				if (data.exists) {
					response.status(403).send({ code: 'data/cedula', message: 'Cedula ya registrada' })
				}
				else {
					console.log("Recibida peticion. Verificando....");
					const captchaToken = request.body['recaptcha'];
					const captchaApiURL = 'https://recaptcha.google.com/recaptcha/api/siteverify';
					const secret = '6Lc5qcEUAAAAALDDjPeHqSPOZl4S4cOvrUZVQR8w'; // functions.config().grecaptcha.serverkey,
					const requestURL = `${captchaApiURL}?secret=${secret}&response=${captchaToken}&remoteip=${request.ip}`;
					rp({
						uri: requestURL,
						method: 'POST',
						json: true,
					})
					.then(res => {
						if (res.success !== true) { // Si el captcha no es validado
							response.status(403).send({ code: 'captcha-verificar', message: 'Captcha no Verificado' });
						}
						else {
							const user = {
								challenge_ts: res.challenge_ts,
								nombres: request.body['nombres'],
								apellidos: request.body['apellidos'],
								telefono: request.body['telefono'],
								cedula: request.body['cedula'],
								ponente: request.body['ponente'],
								trabajaEstudia: request.body['trabajaEstudia'],
								displayName: request.body['nombres'].toString().split(' ')[0] + ' ' + request.body['apellidos'].toString().split(' ')[0]
							}
							console.log('Creando usuario...usuarios/' + request.body['email'])
							db.doc('usuarios/' + request.body['email']).set(user, { merge: false })
							.then(doc => {
								user.telefono = `+58${user.telefono.replace(/^0|-/g, '')}`
								user.uid = request.body['email'],
								user.email = request.body['email'],
								user.emailVerified = false
								user.disabled = false
								user.password = request.body['password']
								admin.auth().createUser(user)
								.then((user) => {
									console.log(`Usuario creado... ${JSON.stringify(user)}`)
									response.status(201).send('Usuario creado');
								})
								.catch(err => {
									console.log(err.errorInfo.code + ': ' + err.errorInfo.message);//'Error al crear cuenta de usuario: ' + JSON.stringify(err))
									response.status(500).send({ code: err.errorInfo.code, message: 'Error al registrar el usuario: ' + err.errorInfo.message })
								});
							})
							.catch(err => {
								console.log('Error al registrar datos de usuario: ' + JSON.stringify(err))
								response.status(500).send({ code: 'firestore-error', message: 'Error al registrar datos el usuario: ' + err.errorInfo.message })
							});
						}
					})
					.catch(error => {
						console.error(error)
						response.status(502).send({ code: 'captcha-error', message: 'Error en la verificacion del captcha: ' + JSON.stringify(error) })
					})
				}
			})
			.catch(error=>{
				response.status(503).send({ code: 'error/cedula', message: 'Error al consultar las cedulas' })
			})
		}
	})
});

exports.removeUserData = functions.auth.user().onDelete(user => {
	console.log(`Eliminando: /usuarios/${user.email}`)
	db.doc(`/usuarios/${user.email}`).delete()
		.then(data => console.log(`Datos del usuario ${user.email} eliminados`))
		.catch(err => console.log(`No se pudo eliminar los datos del usuario ${user.email}, eliminelos manualmente. Error: ${err}`))
})

exports.createTime = functions.firestore.document("/afiches/{idChat}/chat/{idMensaje}").onCreate((doc, contexto)=>{
	doc.ref.update({"timestamp":contexto.timestamp})
})

/*
exports.verifyUser = functions.auth.user().onCreate(user => {
	const userRef = db.collection('usuarios').doc(user.email)
	console.log(`Validando ${userRef.path}`)
	userRef.get()
	.then(data => {
		const doc = data.data()
		// Si el documento existe y no tiene challenge_ts, ya fue validado
		if (data.exists && !doc.challenge_ts) {
			console.log("Usuario ya verificado")
			return "El usuario ya ha sido verificado"
		}
		// Si el documento no existe, el usuario no tiene datos registrados y se elimina
		else if (!data.exists) {
			console.log("El usuario no posee datos registrados")
			admin.auth().deleteUser(user.uid)
				.then(() => {
					return `Usuario invalido eliminado por no poseer datos ${user.uid}`
				})
				.catch((err) => {
					return `Error al eliminar el usuario invalido ${user.uid}: ${err}`
				})
		}
		else // De lo contrario, el usuario si tiene datos registrados con challenge_ts y continua la validacion
		{
			const challenge_ts = moment(doc.challenge_ts).utc() // Hora del desafio
			const challenge_ts2 = moment(doc.challenge_ts).add(15, 'm').utc() // Hora del desafio mas 30 segundos
			const localTime = moment(Date.now()).utc() // Hora actual
			// Si el momento actual NO se encuentra entre el momento del desafio y el momento del desafio mas 30 seg
			const validacion = localTime.isBetween(challenge_ts,challenge_ts2,'seconds')
			var FieldValue = require('firebase-admin').firestore.FieldValue;
			console.log(`Hora Firestore: ${JSON.stringify(FieldValue.serverTimestamp())}`)
			console.log(`La hora actual ${localTime} ${validacion?'esta':'no esta'} entre ${challenge_ts} y ${challenge_ts2}`)
			if (validacion) { // De ser válido se elimina challenge_ts del registro del usuario para marcarlo como verificado
				console.log("Verificando usuario")
				
				userRef.update({ challenge_ts: FieldValue.delete() })
					.then(data => { return `Usuario verificado ${user.email}` })
					.catch(err => { return `Error en la verificacion de usuario: ${err}` })
			}
			else { // De lo contrario se elimina el usuario
				console.log("Captcha invalido")
				admin.auth().deleteUser(user.uid)
					.then(() => { return `Usuario invalido eliminado por no haber validado el captcha ${user.uid}` })
					.catch((err) => { return `Error al eliminar el usuario invalido ${user.uid}: ${err}` })
			}
		}		
	})
	.catch(err => {
		console.log(`Error al consultar al usuario ${user.email}. Su registro será eliminado. Error: ${err}`)
		admin.auth().deleteUser(user.uid)
			.then(() => { return `Usuario invalido eliminado por no haber podido consultar sus datos ${user.uid}` })
			.catch((err) => { return `Error al eliminar el usuario invalido ${user.uid}: ${err}` })
	})

});
*/
